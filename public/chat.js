'use strict';
const socket = io.connect('http://localhost:4000');

let mySocketId;
const divVideoChatLobby = document.getElementById('video-chat-lobby');
const divVideoChat = document.getElementById('video-chat-room');
const joinButton = document.getElementById('join');
const userVideo = document.getElementById('user-video');
const roomInput = document.getElementById('roomName');
let roomName;

let userStream;

const controlButtons = document.getElementById('control-buttons');
const muteAudioButton = document.getElementById('muteAudioButton');
const hideVideoButton = document.getElementById('hideVideoButton');
const leaveRoomButton = document.getElementById('leaveRoomButton');
let hideVideoFlag = false;
let muteAudioFlag = false;

const myRTCPeerConnections = {};

const iceServers = {
  iceServers: [
    {
      urls: 'stun:stun1.l.google.com:19302',
    },
    {
      urls: 'stun:stun2.l.google.com:19302',
    },
    {
      urls: 'stun:stun.1und1.de:3478',
    },
  ],
};

joinButton.addEventListener('click', () => {
  if (roomInput.value === '') {
    alert('Please enter a room name');
    return;
  }

  roomName = roomInput.value;
  console.log(roomName);
  socket.emit('join', roomName);
});

leaveRoomButton.addEventListener('click', () => {
  socket.emit('leave', roomName, mySocketId);
  divVideoChatLobby.style = 'display: block';
  controlButtons.style = 'display: none';

  if (userVideo.srcObject) {
    userVideo.srcObject.getTracks().forEach((track) => track.stop()); // stopping our audio and video streams
  }
  Array.from(document.getElementsByClassName('peer-video')).forEach(
    (peerVideo) => {
      if (peerVideo.srcObject) {
        peerVideo.srcObject.getTracks().forEach((track) => track.stop()); // stopping audio and video streams from peers
      }
    }
  );

  Object.keys(myRTCPeerConnections).forEach((key) => {
    clearMyRTCPeerConnections(key);
  });
});

muteAudioButton.addEventListener('click', () => {
  muteAudioFlag = !muteAudioFlag;
  muteAudioButton.textContent = muteAudioFlag ? 'Unmute' : 'Mute';
  userStream.getTracks()[0].enabled = !muteAudioFlag;
});

hideVideoButton.addEventListener('click', () => {
  hideVideoFlag = !hideVideoFlag;
  hideVideoButton.textContent = hideVideoFlag ? 'Show video' : 'Hide video';
  userStream.getTracks()[1].enabled = !hideVideoFlag;
});

socket.on('connected', (socketId) => {
  if (!mySocketId) {
    mySocketId = socketId;
    console.log('myId: ', mySocketId);
  }
});

socket.on('created', () => {
  getVideo(false);
});

socket.on('joined', () => {
  getVideo(true);
});

socket.on('full', () => {
  alert('The room is full. You cannot join, unfortunately');
});

socket.on('ready', (fromId) => {
  if (fromId === mySocketId) {
    return;
  }
  console.log('Ready', myRTCPeerConnections, fromId);

  setRTCPeerConnection(fromId);

  myRTCPeerConnections[fromId].createOffer().then(
    (offer) => {
      myRTCPeerConnections[fromId].setLocalDescription(offer);
      socket.emit('offer', offer, roomName, mySocketId, fromId);
    },
    (err) => {
      console.log(err);
    }
  );
});

socket.on('candidate', (candidate, fromId, toId) => {
  if (mySocketId !== toId) {
    return;
  }
  console.log('Candidate', myRTCPeerConnections, fromId);
  let iceCandidate = new RTCIceCandidate(candidate);

  myRTCPeerConnections[fromId].addIceCandidate(iceCandidate);
});

socket.on('offer', (offer, fromId, toId) => {
  if (toId !== mySocketId) {
    return;
  }
  console.log('offer', myRTCPeerConnections, fromId, toId);

  setRTCPeerConnection(fromId);

  myRTCPeerConnections[fromId].setRemoteDescription(offer);

  myRTCPeerConnections[fromId].createAnswer().then(
    (answer) => {
      myRTCPeerConnections[fromId].setLocalDescription(answer);
      socket.emit('answer', answer, roomName, mySocketId, fromId);
    },
    (err) => {
      console.log(err);
    }
  );
});

socket.on('answer', (answer, fromId, toId) => {
  if (toId !== mySocketId) {
    return;
  }
  console.log('answer', myRTCPeerConnections, fromId);

  myRTCPeerConnections[fromId].setRemoteDescription(answer);
});

socket.on('leave', (fromId) => {
  if (fromId === mySocketId) {
    return;
  }
  console.log('leave', myRTCPeerConnections, fromId);

  clearMyRTCPeerConnections(fromId);

  const peerVideoToRemove = document.getElementById('peer-video_' + fromId);
  peerVideoToRemove.srcObject.getTracks().forEach((track) => track.stop()); // stopping audio and video streams from peers
  peerVideoToRemove.remove();
});

function getVideo(shouldEmitReady) {
  navigator.mediaDevices
    .getUserMedia({ audio: true, video: { width: 1280, height: 720 } })
    .then((mediaStream) => {
      userStream = mediaStream;

      divVideoChatLobby.style = 'display: none';
      controlButtons.style = 'display: flex';

      userVideo.srcObject = mediaStream;
      userVideo.onloadedmetadata = function (e) {
        userVideo.play();
      };

      if (shouldEmitReady) {
        socket.emit('ready', roomName, mySocketId);
      }
    })
    .catch((err) => {
      alert(err.name + ': ' + err.message);
    });
}

function setRTCPeerConnection(remoteSocketId) {
  const localThis = {
    remoteSocketId,
  };

  if (!myRTCPeerConnections[remoteSocketId]) {
    myRTCPeerConnections[remoteSocketId] = new RTCPeerConnection(iceServers);
  }

  myRTCPeerConnections[
    remoteSocketId
  ].onicecandidate = onICECandidateHandler.bind(localThis);
  myRTCPeerConnections[remoteSocketId].ontrack = onTrackHandler.bind(localThis); // receiving and handling incoming video and audio streams from peer
  myRTCPeerConnections[remoteSocketId].addTrack(
    userStream.getTracks()[0],
    userStream
  ); // sending our audio to peer
  myRTCPeerConnections[remoteSocketId].addTrack(
    userStream.getTracks()[1],
    userStream
  ); // sending our video to peer
}

function onICECandidateHandler(event) {
  if (!event.candidate) {
    return;
  }

  socket.emit(
    'candidate',
    event.candidate,
    roomName,
    mySocketId,
    this.remoteSocketId
  );
}

function onTrackHandler(event) {
  const videoContainer = document.getElementById('video-chat-room');
  const videoId = 'peer-video_' + this.remoteSocketId;
  let newVideoElem = document.getElementById(videoId);

  if (!newVideoElem) {
    newVideoElem = document.createElement('video');
    newVideoElem.id = videoId;
    newVideoElem.classList.add('peer-video');
    videoContainer.appendChild(newVideoElem);
  }

  newVideoElem.srcObject = event.streams[0];
  newVideoElem.onloadedmetadata = function () {
    newVideoElem.play();
  };

  adjustVideoSize();
}

function adjustVideoSize() {
  const numberOfPeerVideos = document.getElementsByClassName('peer-video')
    .length;

  let peerVideoHeight = '90vh';
  if (numberOfPeerVideos > 1 && numberOfPeerVideos < 5) {
    peerVideoHeight = '45vh';
  }

  Array.from(document.getElementsByClassName('peer-video')).forEach(
    (element) => {
      element.style.height = peerVideoHeight;
      element.style.minHeight = peerVideoHeight;
    }
  );
}

function clearMyRTCPeerConnections(connectionKey) {
  myRTCPeerConnections[connectionKey].ontrack = null;
  myRTCPeerConnections[connectionKey].onicecandidate = null;
  myRTCPeerConnections[connectionKey].close();
  delete myRTCPeerConnections[connectionKey];
}
